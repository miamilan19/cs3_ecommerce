import { useState, useEffect } from 'react';
import './App.css';
import Navbar from './components/AppNavbar';
import Landing from './pages/Home';
import Items from './pages/Catalog';
import ItemView from './pages/ItemView';
import Signup from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import AdminPanel from './pages/AdminPanel'
import Invalid from './pages/Error';
import Footer from './components/Footer';

import { UserProvider } from './UserContext';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

function App() {
   const [user, setUser] = useState({
     id: null,
     isAdmin: null
   });

   const unsetUser = () => {
     localStorage.clear();
     setUser({
       id: null,
       isAdmin: null
     })
   }

  useEffect(() =>{
   console.log(user);
     fetch(`https://infinite-reef-88513.herokuapp.com/users/details/`, {
       headers: {
         Authorization: `Bearer ${localStorage.getItem('accessToken')}`
       }
     }).then(res => res.json()).then(convertedResult => {
       console.log(convertedResult);
       if (convertedResult._id !== 'undefined') {
           setUser({
           id: convertedResult._id,
           isAdmin: convertedResult.isAdmin
         })
           
       } else {
         setUser({
           id: null,
           isAdmin: null
       })

     }

   })
  }, [])


  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Navbar />
          <Switch>
            <Route exact path="/" component={Landing}/>
            <Route exact path="/products" component={Items}/>
            <Route exact path="/create" component={AdminPanel}/>
            <Route exact path="/register" component={Signup}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/products/:productId" component={ItemView} />
            <Route component={Invalid} />
          </Switch>
          <Footer> </Footer>
      </Router>
    </UserProvider>
  );
}

export default App;


