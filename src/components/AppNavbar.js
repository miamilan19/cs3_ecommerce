import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext} from 'react';
import '../App.css';

function appNavbar() {
	const {user}  = useContext(UserContext);

	return(
		
		<Navbar className="bg-dark">
			<Container fluid>
				<Navbar.Brand as={NavLink} to="/">ABM Grocery</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse> 
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
					<Nav.Link as={NavLink} to="/create">Create New Product</Nav.Link> 
					
					{(user.id)? <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link> : 
					<>
					<Nav.Link className="ml-auto" as={NavLink} to="/login">Sign in</Nav.Link>
					</>
					}
					
				</Navbar.Collapse>
			</Container>
		</Navbar>

		)
}

export default appNavbar;