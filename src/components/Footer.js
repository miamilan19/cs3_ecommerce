import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import '../App.css';

const Footer = () => <footer className="footerWrapper font-small blue pt-4">
    <div className="footer container-fluid text-md-left">
        <div className="row">
            <div className="col-md-6 mt-md-0 mt-3">
                <h5 className="text-uppercase mt-3">Contact us</h5>
                <p>Email: email@email.com
                </p>
                <Form>
                  <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Or Message us here:</Form.Label>
                    <Form.Control as="textarea" rows={3} />
                  </Form.Group>
                  <Button className="mb-2 btn-dark">Submit</Button>
                </Form>
            </div>

            <hr className="clearfix w-100 d-md-none pb-0"/>

            <div className="col-md-6 mb-md-0 mb-6 text-center">
                <h5 className="text-uppercase mt-3">Follow Us!</h5>
                <ul className="list-unstyled">
                    <li><a href="https://www.facebook.com/" target="_blank"><img src="fb.png"/></a></li>
                    <li><a href="https://www.instagram.com/" target="_blank"><img src="ig.png"/></a></li>
                    <li><a href="https://mail.google.com/mail/u/0/" target="_blank"><img src="twitter.png"/></a></li>
                </ul>
            </div>

        </div>
    </div>

    <div className="page-footer bg-dark footer-copyright text-center py-3">© 2021
        <a> ABM | All Rights Reserved | Privacy Policy</a>
    </div>

</footer>

export default Footer