import Carousel from 'react-bootstrap/Carousel';

export default function Highlights() {

	return(

		<Carousel fade>
		  <Carousel.Item>
		    <img
		      className="img-fluid"
		      src="banner.jpg"
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h3>SUPER SALE</h3>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="img-fluid"
		      src="banner1.jpg"
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h3>PRODUCTS</h3>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="img-fluid"
		      src="banner2.jpg"
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h3>ORDER AND PROCESS PAYMENT ONLINE</h3>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

		)
}

