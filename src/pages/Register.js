import {useState, useEffect} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import {useHistory} from 'react-router-dom';

const createAccountBanner = {
	title: 'Create your account here!',
	tagline: 'Create account and start shopping!' 

}

export default function Register() {
	const history = useHistory(); 
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');	
	const [middleName, setMiddleName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [isRegisterBtnActive, setRegisterBtn] = useState('');
	const [isComplete, setIsComplete] = useState(false); 
    const [isMatched, setIsMatched ] = useState(false);
    const [isValid, setValid] = useState(false);

	function registerUser(event){
	event.preventDefault();
	fetch(`https://infinite-reef-88513.herokuapp.com/users/register`, {
		method: "POST",
		headers: {
			'Content-Type' : 'application/json'

		},
		body: JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			middleName: middleName,
			email: email,
			password: password1,
			mobileNo: mobileNo
		})
	}).then(res => res.json()).then(data=> {
		if (data !== undefined) {
	    	Swal.fire({
	    		title: `Your account is registered successfully!`,
	    		icon: 'success',
	    		text: 'Welcome to online Palengke'
	    	});

    	history.push('/login'); 
    	} else {
    		Swal.fire({
    			title: `Unsuccessful!`,
    			icon: 'error',
    			text: 'Please try again'
    		});
    	}
    
    })	

}

	useEffect(() => { 
	
		if ((firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '') && (password1 === password2) ) {
			 setRegisterBtn(true);
			 setIsComplete(true);
			

		} else {
			setRegisterBtn(false);
			setIsComplete(false);
			
		}
	
	},[firstName, middleName, lastName,email, password1, password2, mobileNo]);

	useEffect(() => {

		if (password1 === password2) {
			setIsMatched(true);
		} else {
			setIsMatched(false);
		}
	}, [password1, password2])

	useEffect(() => {

		if (mobileNo.length === 11) {
			setValid(true);
		} else {	
			setValid(false)
		}
	}, [mobileNo])

	return(
		<Container>
			<Hero data={createAccountBanner}/>
			{
			  isComplete ? 
			    <h1 className="mb-5"> Proceed with Register! </h1>
			  :
			   <h1 className="mb-5"> Fill-out the Form Below </h1>
			}
			{/*Registration Form*/}
			<Form onSubmit={(event) => registerUser(event)} className="mb-5">

				{/*First Name*/}
				<Form.Group className="mb-3"controlId="firstName">
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Insert First Name" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
				</Form.Group>
				{/*Last Name*/}
				<Form.Group className="mb-3"controlId="lastName">
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Insert Last Name" value={lastName} onChange={event => setLastName(event.target.value)} required/>
				</Form.Group>

				{/*Middle Name*/}
				<Form.Group className="mb-3"controlId="middleName">
					<Form.Label>Middle Name:</Form.Label>
					<Form.Control type="text" placeholder="Insert Middle Name" value={middleName} onChange={event => setMiddleName(event.target.value)} required/>
				</Form.Group>


				{/*Email*/}
				<Form.Group className="mb-3"controlId="email">
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Insert Email Address" value={email} onChange={event => setEmail(event.target.value)} required/>
				</Form.Group>
				
				{/*Password1*/}
				<Form.Group className="mb-3"controlId="password1">
					<Form.Label>
						Password:
					</Form.Label>
					<Form.Control type="password" placeholder="Insert Password" value={password1} onChange={event => setPassword1(event.target.value)} required/>
				</Form.Group>

				{isMatched === 'empty'? <p> </p> : isMatched? <p className="text-success"> *Passwords Matched!* </p> : <p className="text-danger"> *Passwords Should Match!* </p>
				}

				<Form.Group className="mb-3"controlId="password2">
					<Form.Label>
						Confirm Password:
					</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={event => setPassword2(event.target.value)} required/>
				</Form.Group>

				<Form.Group className="mb-3"controlId="mobileNo">
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control type="number" placeholder="Insert  Mobile Number"value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
				</Form.Group> 

				{isValid? <p className="text-success"> Valid </p>
				  : <p className="text-danger"> Invalid mobile number </p>}

				{isRegisterBtnActive? <Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">Create New Account</Button> : <Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>Create New Account</Button>}
		
			</Form>
		</Container>

	)
}

