import { Container, Card, Button, Row, Col } from 'react-bootstrap';
export default function ItemView() {

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
						<Card.Title> </Card.Title>
						<Card.Subtitle>Product Description:</Card.Subtitle>
						<Card.Text> </Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP   </Card.Text>

						<Card.Text> </Card.Text>

						<Button variant="warning" block>Add to Cart</Button>

					{/*	<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>*/}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
	}