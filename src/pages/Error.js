import Hero from '../components/Banner';
//we used a named export method to acquire the component from bootstrap.
import { Container } from 'react-bootstrap';

//we must declare a value that we want to pass down for the properties of the Hero Component

let errorMessage = {

   title:"404 - Page Not Found",
   tagline: "You are trying to access a non existing page"
}

//the purpose of this document is to provide as response or component that will be sent back to the client. if the client ever tries to access a route within the app. that does NOT EXISTS.

export default function Error() {
	return(
      <Container>
         <Hero data={errorMessage}/>
      </Container>		
	);
}
