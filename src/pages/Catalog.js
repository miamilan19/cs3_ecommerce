import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import Hero from '../components/Banner';
import Container from 'react-bootstrap/Container'

let info = {
	title:"Welcome to Products Page.",
	tagline:"Shope Now!"
}
export default function Items() {

	const [products, setProducts] = useState([]); 
	useEffect(() => {
		fetch('https://infinite-reef-88513.herokuapp.com/products/').then(outcome => outcome.json()).then(convertedData => {
			console.log(convertedData);
			setProducts(convertedData.map(item => {
				return(
					<ProductCard key={item._id} productInfo={item}/>	
					)
			}));
		});
	})

	return(
		<Container>
			<Hero data={info}/>
			{products}
		</Container>	
		);
}

