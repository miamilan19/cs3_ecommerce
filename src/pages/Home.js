import Hero from '../components/Banner';
import Container from 'react-bootstrap/Container';
import Showcase from '../components/Highlights';


let info = {
	title: "Welcome to ABM Online Grocery!",
	tagline: 'Browse your favorite Products and start Shopping!'
	
}

export default function Home() {
	return(
		<Container>
			<Hero data={info}/>
			<Showcase/>
			
		</Container>

		)
}

