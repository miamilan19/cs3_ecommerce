import UserContext from '../UserContext'
import {useState, useEffect, useContext} from 'react';
import {Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

const bannerLabel = {
	title:"Login your Account",
	tagline:""
}
export default function Login() {

	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const authenticate = (e) => {
		e.preventDefault()

		fetch(`https://infinite-reef-88513.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})

		.then(res => res.json()).then(convertedInformation => {
			console.log(convertedInformation)
			if (convertedInformation.accessToken !== undefined ) {
				localStorage.setItem('accessToken', convertedInformation.accessToken);
				retrieveUserDetails(convertedInformation.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: ""
				});

			} else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again"
				});
			}
		});
	};

	const retrieveUserDetails = (token) => {

		fetch(`https://infinite-reef-88513.herokuapp.com/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(result => result.json()).then(convertedResult => {
			console.log(convertedResult);
			setUser({
				id: convertedResult._id,
				isAdmin: convertedResult.isAdmin
			});
		})
	}
	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {

			setIsActive(false);
		}
	}, [email, password]);
	return(
		(user.id)? <Redirect to="/products"/> : 
		<Container>
			<div className="container">
				{/*Banner of the page*/}
				<Hero data={bannerLabel}/>

				{/*Login Form Component*/}	
				<Form className="mb-2" onSubmit={e => authenticate(e)}>
				{/*User's Email*/}
					<Form.Group className="mb-3" controlId="userEmail">
						<Form.Label> Email Address: </Form.Label>
						<Form.Control type="email" placeholder="Insert Email Here" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>
				{/*User's Password*/}
					<Form.Group className="mb-3" controlId="userPassword">
						<Form.Label>Password: </Form.Label>
						<Form.Control type="Password" placeholder="Insert Password Here" value={password} onChange={e => setPassword(e.target.value)} required/>
					</Form.Group>

				{/*Button Component*/}

				{
					isActive ? <Button type="submit" id="submitBtn" variant="success" block> Login </Button> : <Button  type="submit" id="submitBtn" variant="success" disabled> Login </Button>
				}
				</Form>
				<Form>
				<p>Need account?</p>
				<Button as={NavLink} to="/register" > Sign up here </Button>

				</Form>
				
			</div>		
		</Container>
		);
}


