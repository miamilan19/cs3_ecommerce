import {Form, Button, Container} from 'react-bootstrap';
import {useState} from 'react';


export default function AddItem() {
	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');


	function newItem(event) {
		event.preventDefault();
		// console.log(name);
		// console.log(description);
		// console.log(price);

		fetch(`https://infinite-reef-88513.herokuapp.com/products/create`, {
		method: "POST",
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price,
			})

		}).then(res => res.json()).then(data=> {
			if (data !== 'undefined' ) {
		    	Swal.fire({
		    		title: `Item added Successfuly`,
		    		icon: 'success',
		    		text: 'Click to OK to add more product'
		    	});
	    	} else {
	    		Swal.fire({
	    			title: `Your are UNATHORIZED!`,
	    			icon: 'error',
	    			text: 'Not an Admin'
	    		});
	    	}
	    	console.log(newItem)
	    })	
	}

	return(

		<Container>
			<Form className="m-5">

			{/*Product Name*/}
			  <Form.Group className="mb-3" controlId="name">
			    <Form.Label>Product Name: </Form.Label>
			    <Form.Control type="text" placeholder="Product Name" value={name} onChange={event => setName(event.target.value)} required/>
			  </Form.Group>

			 {/* Product Description*/}
			  <Form.Group className="mb-3" controlId="description">
			    <Form.Label>Product Description:</Form.Label>
			    <Form.Control as="textarea" rows={3} placeholder="Product Description" value={description} onChange={event => setDescription(event.target.value)} required/>
			  </Form.Group>

			  {/*Product Price*/}
			  <Form.Group className="mb-3" controlId="description">
			    <Form.Label>Product Price:</Form.Label>
			    <Form.Control type="number" placeholder="Product Price" value={price} onChange={event => setPrice(event.target.value)} required/>
			  </Form.Group>

			  <Button onClick={(event) => newItem(event)}variant="success" type="submit">
			    Add Item
			  </Button> 
			</Form>
		</Container>
		)
}